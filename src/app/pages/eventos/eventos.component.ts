import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Animal } from '../../models/Animal';
import { Evento } from '../../models/Evento';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DeleteEventoComponent } from '../../dialogs/delete-evento/delete-evento.component'




@Component({
    selector: 'app-eventos',
    templateUrl: 'eventos.component.html',
    styleUrls: ['./eventos.component.scss']
})
export class EventosComponent implements OnInit { 

    modalRef: BsModalRef;
    eventos : Array<Evento> = [];
    search        : string = "";
    loading       : boolean = false;
    public user = { id: '', email: '', name: '', role: '' };

    animalId: any;

    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        public authService: AuthService,
        public router: Router
    ) {}
    
    ngOnInit() {
        
        this.user = this.authService.getUser();
        
        console.log("role: ",this.user.role);
        if(this.user.role==="veterinario"){
            console.log("veterinario")
            this.getEventosDoVeterinario();
        }else if(this.user.role==="responsavelanimais"){
            console.log("responsavel animais");
            this.getEventosDoRespAnimais();
        }else if(this.user.role==="admin"){
            console.log("todos os eventos")
            this.getAllEventos();
        }else{
            console.log("BINGOOOOO");
        }
        
    }
    
    getEventosDoVeterinario() {
        console.log("eventos do veterinario")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Evento[]>('/veterinarios/'+userId+'/eventos')
        .subscribe(
            response => {
                this.loading = false;
                this.eventos = response;
            },
            err => this.handleError(err)
        );
    }


    getEventosDoRespAnimais() {
        console.log("eventos do animal do responsavel de animais")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Evento[]>('/responsaveisanimais/'+userId+'/eventos')
        .subscribe(
            response => {
                this.loading = false;
                this.eventos = response;
            },
            err => this.handleError(err)
        );
    }


    getAllEventos(){
        console.log("todos os eventos")
        this.loading = true;
        this.http.get<Evento[]>('/eventos')
        .subscribe(
            response => {
                this.loading = false;
                this.eventos = response;
            },
            err => this.handleError(err)
        );
    }

    public deleteEvento(idEvento: String) {
        console.log("deleteEvento");
        console.log("idEvento: ", idEvento);

        //se for o veterinario pede permissao para eliminar com o modal
        if(this.user.role === "veterinario"){
            this.modalRef = this.modalService.show(DeleteEventoComponent, {class: 'modal-lg'});
            this.modalRef.content.eventoId = idEvento; 
            //se for o admin elimina direto
        }else if(this.user.role === "admin"){
            this.http.delete('/eventos/'+idEvento)
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Evento anonimizado com sucesso!');
                location.reload();
                this.router.navigate(["/eventos"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }
    }


    
    public deleteEventoFromDb(idEvento: String){
        console.log("idEvento: ", idEvento);

        //valida que é mesmo o admin
        if(this.user.role === "admin"){
            this.http.delete('/eventos/'+idEvento+'/fromdb')
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Evento eliminado com sucesso!');
                location.reload();
                this.router.navigate(["/eventos"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }

    }


     
    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
