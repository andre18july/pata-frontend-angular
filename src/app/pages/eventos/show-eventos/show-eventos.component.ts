import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Evento } from '../../../models/Evento';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-show-eventos',
  templateUrl: './show-eventos.component.html',
  styleUrls: ['./show-eventos.component.scss']
})
export class ShowEventosComponent implements OnInit {

  sub : Subscription;


  eventoId: String = '';
  dataEvento: Date = null;
  veterinarioEvento: String = '';
  animalEvento: String = '';
  tipoEvento: String = '';
  nomeAnimal: String = '';
  nomeVeterinario: String = '';

  obsEvento: String = '';
  ativo: String = '';

  role: String = "";
  teste: String = '';
  evento : Evento = new Evento();
  
  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public generoAnimal = '';

  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.eventoId = params['id'];
      console.log("IDDDDDDD: ", this.eventoId);
    }),

    this.populateForm()
   
  }

  
  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Evento>('/eventos/'+this.eventoId)
    .subscribe(
      response => {
          this.evento = response;
          this.dataEvento = this.evento.data;
          this.veterinarioEvento = this.evento.veterinario;
          this.animalEvento = this.evento.animal;
          this.tipoEvento = this.evento.tipo;
          this.obsEvento = this.evento.observacoes;
          this.nomeAnimal = this.evento.nomeAnimal;
          this.nomeVeterinario = this.evento.nomeVeterinario;
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }


  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


