import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventosComponent } from './eventos.component';
import { CreateEventosComponent } from './create-eventos/create-eventos.component';
import { ShowEventosComponent } from './show-eventos/show-eventos.component'
import { EditEventosComponent } from './edit-eventos/edit-eventos.component'
import { DeleteEventoComponent } from '../../dialogs/delete-evento/delete-evento.component'
import { IntervencoesComponent } from '../intervencoes/intervencoes.component'
import { CreateIntervencoesComponent } from '../intervencoes/create-intervencoes/create-intervencoes.component';
import { ShowIntervencoesComponent } from '../intervencoes/show-intervencoes/show-intervencoes.component'
import { EditIntervencoesComponent } from '../intervencoes/edit-intervencoes/edit-intervencoes.component'
import { DeleteIntervencaoComponent } from '../../dialogs/delete-intervencao/delete-intervencao.component'

import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Eventos'
        },
        children: [{
            path: '',
            component: EventosComponent,
            pathMatch: 'full',
            data: {
                title: 'Lista Eventos',
                requiredRole: ['admin', 'responsavelanimais', 'veterinario']
            },
            canActivate: [AuthGuard]
        },{
            path: 'criar',
            data: {
                title: 'Criar Evento',
                requiredRole: ['admin', 'veterinario']
            },
            component: CreateEventosComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/editar',
            data: {
                title: 'Editar Evento',
                requiredRole: ['admin', 'veterinario']
            },
            component: EditEventosComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/eliminar',
            data: {
                title: 'Eliminar Evento',
                requiredRole: ['admin', 'veterinario']
            },
            component: DeleteEventoComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/mostrar',
            data: {
                title: 'Mostrar Evento',
                requiredRole: ['admin', 'responsavelanimais','veterinario']
            },
            component: ShowEventosComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/intervencoes',
            data: {
                title: 'Mostrar Intervenções',
                requiredRole: ['admin', 'responsavelanimais','veterinario']
            },
            component: IntervencoesComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/criarIntervencao',
            data: {
                title: 'Criar Intervenção',
                requiredRole: ['admin', 'veterinario']
            },
            component: CreateIntervencoesComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/mostrarIntervencao',
            data: {
                title: 'Mostrar Intervenção',
                requiredRole: ['admin', 'responsavelanimais','veterinario']
            },
            component: ShowIntervencoesComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/editarIntervencao',
            data: {
                title: 'Editar Intervenção',
                requiredRole: ['admin', 'responsavelanimais','veterinario']
            },
            component: EditIntervencoesComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/eliminarIntervencao',
            data: {
                title: 'Eliminar Intervenção',
                requiredRole: ['admin', 'veterinario']
            },
            component: DeleteIntervencaoComponent,
            canActivate: [AuthGuard]
        }]
    }
];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EventosRoutingModule {}
