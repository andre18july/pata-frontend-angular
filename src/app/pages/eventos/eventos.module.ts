import { NgModule } from '@angular/core';
import { EventosComponent } from './eventos.component';
import { EventosRoutingModule } from './eventos-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CreateEventosComponent } from './create-eventos/create-eventos.component';
import { ShowEventosComponent } from './show-eventos/show-eventos.component';
import { EditEventosComponent } from './edit-eventos/edit-eventos.component';
import { DeleteEventoComponent } from '../../dialogs/delete-evento/delete-evento.component'
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { IntervencoesComponent } from '../intervencoes/intervencoes.component';
import { CreateIntervencoesComponent } from '../intervencoes/create-intervencoes/create-intervencoes.component';
import { ShowIntervencoesComponent } from '../intervencoes/show-intervencoes/show-intervencoes.component'
import { EditIntervencoesComponent } from '../intervencoes/edit-intervencoes/edit-intervencoes.component'
import { DeleteIntervencaoComponent } from '../../dialogs/delete-intervencao/delete-intervencao.component'





@NgModule({
    imports: [ EventosRoutingModule, SharedModule, AngularDateTimePickerModule ],
    declarations: [
        EventosComponent,
        CreateEventosComponent,
        ShowEventosComponent,
        EditEventosComponent,
        DeleteEventoComponent,
        IntervencoesComponent,
        CreateIntervencoesComponent,
        ShowIntervencoesComponent,
        EditIntervencoesComponent,
        DeleteIntervencaoComponent
    ]
})
export class EventosModule { }
