import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Evento } from '../../../models/Evento'
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-edit-eventos',
  templateUrl: './edit-eventos.component.html',
  styleUrls: ['./edit-eventos.component.scss']
})
export class EditEventosComponent implements OnInit {

  sub : Subscription;

  eventoId: String = '';

  dataEvento: Date = null;
  tipoEvento: String = '';
  observacoesEvento: String = '';


  teste: String = '';

  evento : Evento = new Evento();
  

  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  


  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.eventoId = params['id'];
    }),

    this.populateForm()
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Evento>('/eventos/'+this.eventoId)
    .subscribe(
      response => {
          this.evento = response;
          
          this.dataEvento = this.evento.data;
          this.tipoEvento = this.evento.tipo;
          this.observacoesEvento = this.evento.observacoes;

          this.loading = false;
      },
      err => this.handleError(err)
    );
  }


  updateEvento() {
    
    console.log("Dentro de editEvento");
    this.loading = true;
    let userId = this.user.id;
    this.http.put('/eventos/'+this.eventoId, { 
      data: this.dataEvento, 
      tipo: this.tipoEvento, 

      observacoes: this.observacoesEvento }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Evento registado com sucesso!');
            this.router.navigate(['/eventos']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Ocorreu um erro');
            this.loading = false;
        }
    );
      

  }

  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


