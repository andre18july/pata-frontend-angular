import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Animal } from '../../../models/Animal';

@Component({
  selector: 'app-create-eventos',
  templateUrl: './create-eventos.component.html',
  styleUrls: ['./create-eventos.component.scss']
})
export class CreateEventosComponent implements OnInit {

  idVeterinario: String = '';
  tipo: String = '';
  observacoes: String = '';
  ativo: string = '';

  checkAnimal: boolean;

  animais : Array<Animal> = [];
  animais2 : Array<Animal> = [];
  animal: String = '';

	data: Date = new Date();
	settings = {
		bigBanner: true,
		timePicker: true,
    defaultOpen: false,
    closeOnSelect: true
	}


  loading       : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  constructor(public authService: AuthService, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();
    if(this.user.role==="veterinario"){
      this.getAnimaisDoVeterinario();
    }
    if(this.user.role==="admin"){
      this.getAllAnimaisAtivos();
    }


  }



  newEvento(){
    if (this.user.role === "admin"){
      this.newEventoAdmin();
      
    }else if(this.user.role === "veterinario"){
      this.newEventoVeterinario();
    }
  }

  newEventoVeterinario() {

    
    console.log("Dentro de newEventoVeterinario");
    this.loading = true;
    let userId = this.user.id;

    /*
    if(!this.generoAnimal){
      this.toastr.error('Erro: Genero vazio');
    }else{
    */

    this.http.post('/veterinarios/'+userId+'/eventos', { 
      data: this.data, 
      veterinario: userId,
      animal: this.animal, 
      tipo: this.tipo, 
      observacoes: this.observacoes }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Evento registado com sucesso!');
            this.router.navigate(['/eventos']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.error, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }

  getAnimaisDoVeterinario() {
    console.log("animais do veterinario")
    this.loading = true;
    let userId = this.user.id;
    this.http.get<Animal[]>('/veterinarios/'+userId+'/animais')
    .subscribe(
        response => {
            this.loading = false;
            this.animais = response;
        },
        err => this.handleError(err)
    );
  }

  getAllAnimaisAtivos(){
    console.log("todos os animais ativos")
    this.loading = true;
    this.http.get<Animal[]>('/animais/ativos')
    .subscribe(
        response => {
            this.loading = false;
            this.animais2 = response;
        },
        err => this.handleError(err)
    );
  }


  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }
    


  newEventoAdmin() {

    
    console.log("Dentro de newEventoAdmin");
    this.loading = true;
    let userId = this.user.id;

    /*
    if(!this.generoAnimal){
      this.toastr.error('Erro: Genero vazio');
    }else{
    */

    this.http.post('/eventos', { 
      data: this.data, 
      veterinario: this.idVeterinario,
      animal: this.animal, 
      tipo: this.tipo, 
      observacoes: this.observacoes }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Evento registado com sucesso!');
            this.router.navigate(['/eventos']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.error, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }

}


