import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEventosComponent } from './create-eventos.component';

describe('CreateEventosComponent', () => {
  let component: CreateEventosComponent;
  let fixture: ComponentFixture<CreateEventosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEventosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEventosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
