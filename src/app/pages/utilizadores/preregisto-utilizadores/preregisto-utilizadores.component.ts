import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Utilizador } from '../../../models/Utilizador';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-preregisto-utilizadores',
  templateUrl: './preregisto-utilizadores.component.html',
  styleUrls: ['./preregisto-utilizadores.component.scss']
})
export class PreregistoUtilizadoresComponent implements OnInit {

  modalRef: BsModalRef;
  utilizadores : Array<Utilizador> = [];
  search        : string = "";
  loading       : boolean = false;
  public user = { id: '', email: '', name: '', role: '' };


  sub : Subscription;

  utilizador : Utilizador = new Utilizador();
  utilizadorId: String = '';

  nomeUtilizador: String = '';
  emailUtilizador: String = '';
  roleUtilizador: String = '';
  ativoUtilizador: boolean = false;

  role: String = '';



  constructor(
    private http: HttpClient, 
    private toastr: ToastrService,
    private modalService: BsModalService,
    public authService: AuthService,
    public router: Router,
    private activeRoute: ActivatedRoute
  ) { }


  ngOnInit() {

    this.user = this.authService.getUser();
    this.getUsersPreRegisto();

  }


  getUsersPreRegisto(){
    console.log("list users pre registo")
    this.loading = true;
    this.http.get<Utilizador[]>('/utilizadores/listpreregisto')
    .subscribe(
        response => {
            console.log(response)
            this.loading = false;
            this.utilizadores = response;
        },
        err => this.handleError(err)
    );

  }

  validarUtilizador(idUtilizador: String){
    console.log("idUtilizador: ", idUtilizador)

    if(this.role==='admin')
    {
      console.log("user e admin")
      this.toastr.error('Não é possível alterar o utilizador Admin pela UI');
      this.loading = false;
    }else{
      
      console.log("Dentro de valida user");
      this.loading = true;
      let userId = this.user.id;
      this.http.put('/utilizadores/'+idUtilizador+'/alteraestado', { 


   
       }).subscribe(
          (res: any) => {
                        
              this.toastr.success('Utilizador ativado com sucesso!');
              this.router.navigate(['/utilizadores']);
              this.loading = false;
          },
          err => {
              this.toastr.error(err.error.message, 'Ocorreu um erro');
              this.loading = false;
          }
      );

    }
  }


  rejeitarUtilizador(idUtilizador: String){
    console.log("idUtilizador: ", idUtilizador)

    if(this.role==='admin')
    {
      console.log("user e admin")
      this.toastr.error('Não é possível alterar o utilizador Admin pela UI');
      this.loading = false;
    }else{
      
      console.log("Dentro de rejeita user");
      this.loading = true;
      let userId = this.user.id;
      this.http.put('/utilizadores/'+idUtilizador+'/rejeitauser/rejeita', { 

       }).subscribe(
          (res: any) => {
                        
              this.toastr.success('Utilizador notificado com sucesso!');
              this.router.navigate(['/utilizadores']);
              this.loading = false;
          },
          err => {
              this.toastr.error(err.error.message, 'Ocorreu um erro');
              this.loading = false;
          }
      );

    }
  }


  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


