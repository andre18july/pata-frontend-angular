import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreregistoUtilizadoresComponent } from './preregisto-utilizadores.component';

describe('CreateAnimaisComponent', () => {
  let component: PreregistoUtilizadoresComponent;
  let fixture: ComponentFixture<PreregistoUtilizadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreregistoUtilizadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreregistoUtilizadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
