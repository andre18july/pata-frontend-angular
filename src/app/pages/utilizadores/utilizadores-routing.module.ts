import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UtilizadoresComponent } from './utilizadores.component';
import { EditUtilizadoresComponent } from './edit-utilizadores/edit-utilizadores.component';
import { ShowUtilizadoresComponent } from './show-utilizadores/show-utilizadores.component';
import { DeleteUtilizadorComponent } from '../../dialogs/delete-utilizador/delete-utilizador.component';
import { PreregistoUtilizadoresComponent } from './preregisto-utilizadores/preregisto-utilizadores.component';
import { ShowCredComponent } from '../../dialogs/show-cred/show-cred.component';
import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Utilizadores'
        },
        children: [{
            path: '',
            component: UtilizadoresComponent,
            pathMatch: 'full',
            data: {
                title: 'Lista Utilizadores',
                requiredRole: ['admin']
            },
            canActivate: [AuthGuard]
        },{
            path: ':id/editar',
            data: {
                title: 'Editar Utilizador',
                requiredRole: ['admin']
            },
            component: EditUtilizadoresComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/eliminar',
            data: {
                title: 'Eliminar Utilizador',
                requiredRole: ['admin']
            },
            component: DeleteUtilizadorComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/mostrar',
            data: {
                title: 'Mostrar Utilizador',
                requiredRole: ['admin']
            },
            component: ShowUtilizadoresComponent,
            canActivate: [AuthGuard]
            
        },{
            path: ':id/:p/mostrar2',
            data: {
                title: 'Mostrar Utilizador',
                requiredRole: ['admin']
            },
            component: ShowUtilizadoresComponent,
            canActivate: [AuthGuard]
            
        },{
            path: 'listaPreRegisto',
            data: {
                title: 'Validar Utilizador',
                requiredRole: ['admin']
            },
            component: PreregistoUtilizadoresComponent,
            canActivate: [AuthGuard]
        },{
            path: ':showcred',
            data: {
                title: 'Show Cred',
                requiredRole: ['admin']
            },
            component: ShowCredComponent,
            canActivate: [AuthGuard]
        }]
    }
];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class UtilizadoresRoutingModule {}
