import { NgModule } from '@angular/core';

import { UtilizadoresComponent } from './utilizadores.component';

import { UtilizadoresRoutingModule } from './utilizadores-routing.module';


import { EditUtilizadoresComponent } from './edit-utilizadores/edit-utilizadores.component';
import { ShowUtilizadoresComponent } from './show-utilizadores/show-utilizadores.component';
import { DeleteUtilizadorComponent } from '../../dialogs/delete-utilizador/delete-utilizador.component';
import { PreregistoUtilizadoresComponent } from '../utilizadores/preregisto-utilizadores/preregisto-utilizadores.component'

import { SharedModule } from '../../shared/shared.module';

import { ShowCredComponent } from '../../dialogs/show-cred/show-cred.component'


@NgModule({
    imports: [ UtilizadoresRoutingModule, SharedModule ],
    declarations: [
        UtilizadoresComponent,
        EditUtilizadoresComponent,
        ShowUtilizadoresComponent,
        DeleteUtilizadorComponent,
        PreregistoUtilizadoresComponent,
        ShowCredComponent
    ]
})
export class UtilizadoresModule { }
