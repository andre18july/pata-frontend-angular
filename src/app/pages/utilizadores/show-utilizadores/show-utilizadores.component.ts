import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Utilizador } from '../../../models/Utilizador';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ShowCredComponent } from '../../../dialogs/show-cred/show-cred.component'

@Component({
  selector: 'app-show-utilizadores',
  templateUrl: './show-utilizadores.component.html',
  styleUrls: ['./show-utilizadores.component.scss']
})
export class ShowUtilizadoresComponent implements OnInit {

  sub : Subscription; 
  modalRef: BsModalRef;


  utilizadorId: String = '';

  nomeUtilizador: String = '';
  emailUtilizador: String = '';
  roleUtilizador: String = '';
  credencialUtilizador: String = '';
  estadoUtilizador: String = '';

  p: String = '';


  utilizador : Utilizador = new Utilizador();
  

  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public selectOptions = [
    "VET",
    "TRN"
  ];



  constructor(public authService: AuthService, private modalService: BsModalService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    
    this.sub = this.activeRoute.params.subscribe(params => {
      console.log("IDDDDDDD: ", params['id']);
      this.utilizadorId = params['id'];
      console.log("PARAM P: ", params['p'])
      this.p = params['p'];
    }),

    this.populateForm()
   
  }

  verCred(event: any){
    console.log("dentro de ver cred");
    this.modalRef = this.modalService.show(ShowCredComponent, {class: 'modal-lg'});
    console.log("Dentro do ver cred - credencial do user: ", this.utilizador)
    this.modalRef.content.credencial = this.credencialUtilizador;
    this.modalRef.content.utilizador = this.utilizador;
  }

  validarUtilizador(idUtilizador: String){
    console.log("idUtilizador: ", idUtilizador)

    if(this.roleUtilizador==='admin')
    {
      console.log("user e admin")
      this.toastr.error('Não é possível alterar o utilizador Admin pela UI');
      this.loading = false;
    }else{
      
      console.log("Dentro de valida user");
      this.loading = true;
      let userId = this.user.id;
      this.http.put('/utilizadores/'+idUtilizador+'/alteraestado', { 


   
       }).subscribe(
          (res: any) => {
                        
              this.toastr.success('Utilizador ativado com sucesso!');
              this.router.navigate(['/utilizadores']);
              this.loading = false;
          },
          err => {
              this.toastr.error(err.error.message, 'Ocorreu um erro');
              this.loading = false;
          }
      );

    }
  }




  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Utilizador>('/utilizadores/'+this.utilizadorId)
    .subscribe(
      response => {
          this.utilizador = response;
          this.nomeUtilizador = this.utilizador.name;
          this.emailUtilizador = this.utilizador.email;
          this.roleUtilizador = this.utilizador.role;
          this.credencialUtilizador = this.utilizador.credencial;
          this.estadoUtilizador = this.utilizador.ativo;
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }



  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


