import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUtilizadoresComponent } from './show-utilizadores.component';

describe('CreateAnimaisComponent', () => {
  let component: ShowUtilizadoresComponent;
  let fixture: ComponentFixture<ShowUtilizadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowUtilizadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUtilizadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
