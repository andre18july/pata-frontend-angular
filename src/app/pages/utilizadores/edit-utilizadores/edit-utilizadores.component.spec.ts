import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditUtilizadoresComponent } from './edit-utilizadores.component';

describe('EditUtilizadoresComponent', () => {
  let component: EditUtilizadoresComponent;
  let fixture: ComponentFixture<EditUtilizadoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditUtilizadoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditUtilizadoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
