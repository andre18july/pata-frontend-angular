import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Utilizador } from '../../../models/Utilizador';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-edit-utilizadores',
  templateUrl: './edit-utilizadores.component.html',
  styleUrls: ['./edit-utilizadores.component.scss']
})
export class EditUtilizadoresComponent implements OnInit {

  sub : Subscription;

  utilizadorId: String = '';

  nomeUtilizador: String = '';
  emailUtilizador: String = '';
  roleUtilizador: String = '';

  role: String = '';


  utilizador : Utilizador = new Utilizador();
  

  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public selectOptions = [
    "VET",
    "TRN"
  ];


  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.utilizadorId = params['id'];
    }),

    this.populateForm()
    
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Utilizador>('/utilizadores/'+this.utilizadorId)
    .subscribe(
      response => {
          this.utilizador = response;
          this.nomeUtilizador = this.utilizador.name;
          this.emailUtilizador = this.utilizador.email;
          this.roleUtilizador = this.utilizador.role;
          this.role = this.utilizador.role;
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }


  updateUtilizador() {


    if(this.role==='admin')
    {
      console.log("user e admin")
      this.toastr.error('Não é possível alterar o utilizador Admin pela UI');
      this.loading = false;
    }else{
      
      console.log("Dentro de editUtilizador");
      this.loading = true;
      let userId = this.user.id;
      this.http.put('/utilizadores/'+this.utilizadorId, { 
        name: this.nomeUtilizador, 
        email: this.emailUtilizador, 

   
       }).subscribe(
          (res: any) => {
                        
              this.toastr.success('Utilizador alterado com sucesso!');
              this.router.navigate(['/utilizadores']);
              this.loading = false;
          },
          err => {
              this.toastr.error(err.error.message, 'Ocorreu um erro');
              this.loading = false;
          }
      );
      
    }
  }

  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


