import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Utilizador } from '../../models/Utilizador';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DeleteUtilizadorComponent } from '../../dialogs/delete-utilizador/delete-utilizador.component'
import { Subscription } from 'rxjs/Subscription';



@Component({
    selector: 'app-utilizadores',
    templateUrl: 'utilizadores.component.html'
})
export class UtilizadoresComponent implements OnInit { 

    modalRef: BsModalRef;
    utilizadores : Array<Utilizador> = [];
    search        : string = "";
    loading       : boolean = false;
    public user = { id: '', email: '', name: '', role: '' };


    sub : Subscription;

    utilizador : Utilizador = new Utilizador();
    utilizadorId: String = '';
  
    nomeUtilizador: String = '';
    emailUtilizador: String = '';
    roleUtilizador: String = '';
    ativoUtilizador: boolean = false;
  
    role: String = '';

    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        public authService: AuthService,
        public router: Router,
        private activeRoute: ActivatedRoute

    ) {}
    
    ngOnInit() {

        this.user = this.authService.getUser();
        this.getAllUtilizadores();

    }
    

    getAllUtilizadores(){
        console.log("todos os utilizadores")
        this.loading = true;
        this.http.get<Utilizador[]>('/utilizadores')
        .subscribe(
            response => {
                console.log(response)
                this.loading = false;
                this.utilizadores = response;
            },
            err => this.handleError(err)
        );
    }




  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Utilizador>('/utilizadores/'+this.utilizadorId)
    .subscribe(
      response => {
          this.utilizador = response;
          this.nomeUtilizador = this.utilizador.name;
          this.emailUtilizador = this.utilizador.email;
          this.roleUtilizador = this.utilizador.role;
          this.role = this.utilizador.role;
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }


    public deleteUtilizador(idUtilizador: String) {
        console.log("deleteUtilizador");
        console.log("idUtilizador: ", idUtilizador);
        
        this.utilizadorId = idUtilizador;
        this.populateForm();
 
        setTimeout(() => {
            console.log("role: ", this.role);

            if(this.user.role === "admin"){

                if(this.role==="responsavelanimais"){
                    console.log("dentro de eliminar respanimais");

                    this.http.delete('/responsaveisanimais/'+idUtilizador)
                    .subscribe(
                    (res: any) => {                    
                        this.toastr.success('Utilizador anonimizado com sucesso!');
                        location.reload();
                        this.router.navigate(["/utilizadores"]);
                    },
                    err => {
                        this.toastr.error(err.error.message, 'Ocorreu um erro');
                    }
                    );
                }else if(this.role==="treinador"){
                    console.log("dentro de eliminar treinador");
                    this.http.delete('/treinadores/'+idUtilizador)
                    .subscribe(
                    (res: any) => {                    
                        this.toastr.success('Utilizador anonimizado com sucesso!');
                        location.reload();
                        this.router.navigate(["/utilizadores"]);
                    },
                    err => {
                        this.toastr.error(err.error.error, 'Ocorreu um erro: ');
                    }
                    );

                }else if(this.role==="veterinario"){
                    console.log("dentro de eliminar vet");
                }
                
            }else{
                this.toastr.error('Ação negada: não tem permissões');
            }
        }, 1000);
    }

    /*
     
    public deleteAnimalFromDb(idAnimal: String){
        console.log("idAnimal: ", idAnimal);

        //valida que é mesmo o admin
        if(this.user.role === "admin"){
            this.http.delete('/animais/'+idAnimal+'/fromdb')
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Animal eliminado com sucesso!');
                location.reload();
                this.router.navigate(["/animais"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }

    }
     
    */

    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
