import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OcorrenciasComponent } from './ocorrencias.component';
import { CreateOcorrenciasComponent } from './create-ocorrencias/create-ocorrencias.component';
import { ShowOcorrenciasComponent } from './show-ocorrencias/show-ocorrencias.component';
import { ShowFotoComponent } from '../../dialogs/show-foto/show-foto.component';



import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Ocorrencias'
        },
        children: [{
            path: '',
            component: OcorrenciasComponent,
            pathMatch: 'full',
            data: {
                title: 'Lista Ocorrências',
                requiredRole: ['admin', 'responsavelanimais']
            },
            canActivate: [AuthGuard]
        },{
            path: ':id/mostrar',
            data: {
                title: 'Mostrar Treino',
                requiredRole: ['admin', 'responsavelanimais']
            },
            component: ShowOcorrenciasComponent,
            canActivate: [AuthGuard]
        },{
            path: 'criar',
            data: {
                title: 'Criar Ocorrencia',
            },
            component: CreateOcorrenciasComponent,
        },{
            path: ':showfoto',
            data: {
                title: 'Show Foto',
                requiredRole: ['admin', 'responsavelanimais']
            },
            component: ShowFotoComponent,
            canActivate: [AuthGuard]
        }]
    }
];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OcorrenciasRoutingModule {}
