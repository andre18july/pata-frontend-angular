import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Animal } from '../../models/Animal';
import { Ocorrencia } from '../../models/Ocorrencia';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DeleteTreinoComponent } from '../../dialogs/delete-treino/delete-treino.component'




@Component({
    selector: 'app-ocorrencias',
    templateUrl: 'ocorrencias.component.html',
    styleUrls: ['./ocorrencias.component.scss']
})
export class OcorrenciasComponent implements OnInit { 

    modalRef: BsModalRef;
    ocorrencias : Array<Ocorrencia> = [];
    search        : string = "";
    loading       : boolean = false;
    public user = { id: '', email: '', name: '', role: '' };

    animalId: any;

    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        public authService: AuthService,
        public router: Router
    ) {}
    
    ngOnInit() {
        
        this.user = this.authService.getUser();
        
        console.log("role: ",this.user.role);

        if(this.user.role==="responsavelanimais"){
            console.log("responsavel animais");
            this.getOcorrenciasDoRespAnimais();
        }else if(this.user.role==="admin"){
            console.log("todos os animais")
            this.getAllOcorrencias();
        }else{
            console.log("BINGOOOOO");
        }
        
    }
    

    getOcorrenciasDoRespAnimais() {
        console.log("Ocorrencias do animal do responsavel de animais")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Ocorrencia[]>('/responsaveisanimais/'+userId+'/ocorrencias')
        .subscribe(
            response => {
                this.loading = false;
                this.ocorrencias = response;
            },
            err => this.handleError(err)
        );
    }


    getAllOcorrencias(){
        console.log("Todas as ocorrencias")
        this.loading = true;
        this.http.get<Ocorrencia[]>('/ocorrencias')
        .subscribe(
            response => {
                this.loading = false;
                this.ocorrencias = response;
            },
            err => this.handleError(err)
        );
    }



     
    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
