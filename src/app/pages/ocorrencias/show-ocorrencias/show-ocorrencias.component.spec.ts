import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowOcorrenciasComponent } from './show-ocorrencias.component';

describe('CreateOcorrenciasComponent', () => {
  let component: ShowOcorrenciasComponent;
  let fixture: ComponentFixture<ShowOcorrenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowOcorrenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowOcorrenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
