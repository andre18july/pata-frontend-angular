import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Treino } from '../../../models/Treino';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { Ocorrencia } from '../../../models/Ocorrencia';
import { BsModalRef } from 'ngx-bootstrap';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ShowFotoComponent } from '../../../dialogs/show-foto/show-foto.component'

@Component({
  selector: 'app-show-ocorrencias',
  templateUrl: './show-ocorrencias.component.html',
  styleUrls: ['./show-ocorrencias.component.scss']
})
export class ShowOcorrenciasComponent implements OnInit {


  modalRef: BsModalRef;
  sub : Subscription;

  ocorrencia : Ocorrencia = new Ocorrencia();

  ocorrenciaId: String = '';
  tipo: String = 'maustratos';
  data: Date = null;
  animais : Array<Animal> = [];
  animais2 : Array<Animal> = [];
  responsavelAnimais: String = '';
  responsabilidade: boolean;
  localizacao: String = '';
  lat: string = '';
  long: string = '';
  estado: String = '';
  tipoAnimal: String = '';
  racaAnimal: String = '';
  fotoAnimal: String = '';
  descricao: String = '';
  animal: String = '';
  contacto: String = '';
  agressor: String = '';
  notificada: Boolean = false;
  ativo:      Boolean;

  role: String = "";
  teste: String = '';
  treino : Treino = new Treino();
  
  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  


  constructor(public authService: AuthService,
    private modalService: BsModalService,
    private activeRoute: ActivatedRoute, 
    private http: HttpClient, 
    private toastr: ToastrService, 
    private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.ocorrenciaId = params['id'];
      console.log("IDDDDDDD: ", this.ocorrenciaId);
    }),

    this.populateForm()
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Ocorrencia>('/ocorrencias/'+this.ocorrenciaId)
    .subscribe(
      response => {
          this.ocorrencia = response;
          this.data = this.ocorrencia.data;
          this.tipo = this.ocorrencia.tipo;
          this.animal = this.ocorrencia.animal;
          this.responsavelAnimais = this.ocorrencia.responsavelAnimais;
          this.descricao = this.ocorrencia.descricao;
          this.localizacao = this.ocorrencia.localizacao;
          this.lat = this.ocorrencia.lat;
          this.long = this.ocorrencia.long;
          this.estado = this.ocorrencia.estadoAnimal;
          this.tipoAnimal = this.ocorrencia.tipoAnimal;
          this.fotoAnimal = this.ocorrencia.fotoAnimal;
          this.racaAnimal = this.ocorrencia.racaAnimal;
          this.contacto = this.ocorrencia.contacto;
          this.agressor = this.ocorrencia.agressor;
          this.contacto = this.ocorrencia.contacto;
          this.notificada = this.ocorrencia.notificada;
          this.ativo = this.ocorrencia.ativo;
   

          this.loading = false;
      },
      err => this.handleError(err)
    );
  }

  verFoto(event: any){
    console.log("dentro de ver foto");
    this.modalRef = this.modalService.show(ShowFotoComponent, {class: 'modal-lg'});
    this.modalRef.content.fotoAnimal = this.fotoAnimal;
    console.log("foto: ",this.fotoAnimal);
    this.modalRef.content.ocorrencia = this.ocorrencia;
  }



  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


