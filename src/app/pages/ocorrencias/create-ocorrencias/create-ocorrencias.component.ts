import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-create-ocorrencias',
  templateUrl: './create-ocorrencias.component.html',
  styleUrls: ['./create-ocorrencias.component.scss']
})
export class CreateOcorrenciasComponent implements OnInit {

  tipo: String = 'maustratos';
  data: Date = null;
  animais : Array<Animal> = [];
  animais2 : Array<Animal> = [];
  responsabilidade: boolean;
  localizacao: String = '';
  lat: number = 0;
  long: number = 0;
  estado: String = '';
  tipoAnimal: String = '';
  racaAnimal: String = '';
  fotoAnimal: string = '';
  descricao: String = '';
  animal: String = '';
  contacto: String = '';
  agressor: String = '';


  //file upload
  base64File: SafeUrl = '';
  fileToUpload: File = null;
  
  

  loading: boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  constructor(public authService: AuthService, private http: HttpClient, private toastr: ToastrService, private router: Router, private domSanitizer: DomSanitizer) { 

  }

  ngOnInit() {
    this.user = this.authService.getUser();
    if(this.user.role==="responsavelanimais"){
      this.getAnimaisDoResponsavel();
    }
    if(this.user.role==="admin"){
      this.getAllAnimaisAtivos();
    }
    this.getLocation();
    this.base64File = this.domSanitizer.bypassSecurityTrustUrl(this.fotoAnimal);

  }

  fileChangeEvent(fileInput: any){
    //this.fileToUpload = <File> fileInput.target.file;  
    this.fileToUpload = fileInput.target.files[0];
    let reader = new FileReader();
    let data = {};
    reader.readAsDataURL(this.fileToUpload);
      reader.onload = () => {
       this.base64File = reader.result.split(',')[1]  
      };
  }


  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }

  getAnimaisDoResponsavel() {
    console.log("animais do responsavel")
    this.loading = true;
    let userId = this.user.id;
    this.http.get<Animal[]>('/responsaveisanimais/'+userId+'/animais')
    .subscribe(
        response => {
            this.loading = false;
            this.animais = response;
        },
        err => this.handleError(err)
    );
  }

  getAllAnimaisAtivos(){
    console.log("todos os animais ativos")
    this.loading = true;
    this.http.get<Animal[]>('/animais/ativos')
    .subscribe(
        response => {
            this.loading = false;
            this.animais2 = response;
        },
        err => this.handleError(err)
    );
  }


  newOcorrencia(){
    if (this.user.role === "admin"){
      this.newOcorrenciaAdmin();
      
    }else if(this.user.role === "responsavelanimais"){
      this.newOcorrenciaRespAnimais();
    }else if(!this.authService.isAuthenticated()){
      this.newOcorrenciaAdmin();
    }
  }




  newOcorrenciaRespAnimais() {

    console.log("Dentro de newOcorrenciaRespAnimais");
    this.loading = true;
    let userId = this.user.id;

    this.http.post('/responsaveisanimais/'+userId+'/ocorrencias', { 
      tipo: this.tipo,
      tipoAnimal: this.tipoAnimal,
      racaAnimal: this.racaAnimal,
      localizacao: this.localizacao,
      lat: this.lat,
      long: this.long,
      descricao: this.descricao,
      animal: this.animal,
      estadoAnimal: this.estado,
      fotoAnimal: this.base64File,
      contacto: this.contacto,
      agressor: this.agressor,
     }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Ocorrencia registada com sucesso!');
            this.router.navigate(['/ocorrencias']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }
    

  newOcorrenciaAdmin() {

    console.log("Dentro de newOcorrenciaAdmin");
    this.loading = true;
    let userId = this.user.id;

    this.http.post('/ocorrencias/', { 
      tipo: this.tipo,
      tipoAnimal: this.tipoAnimal,
      racaAnimal: this.racaAnimal,
      localizacao: this.localizacao,
      lat: this.lat,
      long: this.long,
      descricao: this.descricao,
      animal: this.animal,
      estadoAnimal: this.estado,
      fotoAnimal: this.base64File,
      contacto: this.contacto,
      agressor: this.agressor,
     }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Ocorrencia registada com sucesso!');
            if(this.user.role==="admin"){
              //location.reload();
              this.router.navigate(['/ocorrencias']);
            }else if(!this.authService.isAuthenticated()){
              //location.reload();
              this.router.navigate(['/']);
            }
            
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }

  getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((position) => {
          this.lat = position.coords.latitude;
          this.long = position.coords.longitude;
          console.log("Latitude: " + this.lat);
          console.log("Longitude: " + this.long); 
        });
    } else {
        console.log("Geolocation is not supported by this browser.");
    }
  }


  
  
}




