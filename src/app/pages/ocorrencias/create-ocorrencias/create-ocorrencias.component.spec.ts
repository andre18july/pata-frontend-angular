import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOcorrenciasComponent } from './create-ocorrencias.component';

describe('CreateOcorrenciasComponent', () => {
  let component: CreateOcorrenciasComponent;
  let fixture: ComponentFixture<CreateOcorrenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOcorrenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOcorrenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
