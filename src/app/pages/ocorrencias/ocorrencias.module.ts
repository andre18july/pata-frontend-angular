import { NgModule } from '@angular/core';
import { OcorrenciasComponent } from './ocorrencias.component';
import { OcorrenciasRoutingModule } from './ocorrencias-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CreateOcorrenciasComponent } from './create-ocorrencias/create-ocorrencias.component';
import { ShowOcorrenciasComponent } from './show-ocorrencias/show-ocorrencias.component';
import { ShowFotoComponent } from '../../dialogs/show-foto/show-foto.component'



@NgModule({
    imports: [ OcorrenciasRoutingModule, SharedModule ],
    declarations: [
        OcorrenciasComponent,
        CreateOcorrenciasComponent,
        ShowOcorrenciasComponent,
        ShowFotoComponent

    ]
})
export class OcorrenciasModule { }
