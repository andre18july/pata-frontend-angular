import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { forEach } from '@angular/router/src/utils/collection';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  templateUrl: 'register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent {

  errors: Array<string> = [];
  email: string = '';
  name: string = '';
  password: string = '';
  passwordConfirmation: string = '';
  role: string = '';
  acordo: boolean;
  loading: boolean = false;

  //treinador e veterinario
  credencial: string = '';
  morada: string = '';
  contacto: string = '';

  //file upload
  base64File: SafeUrl = '';
  fileToUpload: File = null;



  list: any = [
    {id: "RDA", name: 'Responsável de Animais', value: 'RDA'},
    {id: "TRN", name: 'Treinador', value: 'TRN'},
    {id: "VET", name: 'Veterinário', value: 'VET'}
  ];
  tipoconta : string = "RDA";



  //veterinaria
  especialidade: string ="";
  horario : string = "";


  listEspecialidade: any = [
    {id: "E1", name: 'Cardiologia', value: 'Cardiologia'},
    {id: "E2", name: 'Ortopedia', value: 'Ortopedia'},
    {id: "E3", name: 'Cirurgia', value: 'Cirurgia'}
  ];


  listHorario: any = [
    {id: "H1", name: 'Serviço 24h', value: 'Serviço 24h'},
    {id: "H2", name: 'Aberto todos os dias: 9h - 19h', value: 'Aberto todos os dias: 9h - 19h'},
    {id: "H3", name: 'Aberto dias da semana:  9h - 19h', value: 'Aberto dias da semana:  9h - 19h'}
  ];




  constructor(public authService: AuthService, private http: HttpClient, private domSanitizer: DomSanitizer, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.base64File = this.domSanitizer.bypassSecurityTrustUrl(this.credencial);

  }




  register() {

    if(this.tipoconta==="RDA"){
      console.log("registo de responsavelanimais")
      this.registerResponsavelAnimais();
    }else if(this.tipoconta==="TRN"){
      console.log("registo de treinador")
      this.registerTreinador();
    }else if(this.tipoconta==="VET"){
      console.log("registo de veterinario")
      this.registerVeterinario(); 
    }else{
      this.toastr.error('Tipo de utilizador inválido');
    }
  }



  logDropdown(): void {
    const NAME = this.list.find((item: any) => item.id === this.tipoconta).name;
  }

  logDropdown2(): void {
    const NAME = this.listEspecialidade.find((item: any) => item.value === this.especialidade).name;
  }

  logDropdown3(): void {
    const NAME = this.listHorario.find((item: any) => item.value === this.horario).name;
    console.log("logdrop3", NAME)
  }


  registerResponsavelAnimais() {

    console.log("Register Responsavel Animais")
    this.loading = true;
    this.errors = [];


    if (this.password != this.passwordConfirmation) {
      this.errors.push('As Passwords não são iguais');
      this.loading = false;
      return;
    }

    console.log("this.role ", this.role)

    this.http.post('auth/registerResponsavelAnimais', { email: this.email, name: this.name, password: this.password,  role: 'responsavelanimais' }).subscribe(
      (res: any) => {
        this.authService.setToken(res.token);
        this.toastr.success('Conta criada com sucesso!');
        this.router.navigate(['/']);
        this.loading = false;
      },
      err => {
        this.toastr.error(err.error.message, 'Ocorreu um erro');
        this.loading = false;
      }
    );
  }




  registerTreinador() {

    console.log("Register Treinador")
    this.loading = true;
    this.errors = [];


    if (this.password != this.passwordConfirmation) {
      this.errors.push('As Passwords não são iguais');
      this.loading = false;
      return;
    }

    if (this.morada === ""){
      this.errors.push("A morada tem de ser preenchida");
      this.loading = false;
      return;
    }

    if(this.contacto === ""){
      this.errors.push("O contacto tem de ser preenchido");
      this.loading = false;
      return;
    }

    console.log("this.role ", this.role)

    this.http.post('auth/registerTreinador', { email: this.email, 
      name: this.name, password: this.password,  
      role: 'treinador', 
      credencial: this.base64File, 
      morada: this.morada, 
      contacto: this.contacto }).subscribe(
      (res: any) => {
        this.authService.setToken(res.token);
        this.toastr.success('Conta criada com sucesso!');
        this.authService.removeToken();
        location.reload();
        this.router.navigate(["/"]);
        this.loading = false;
      },
      err => {
        console.log(err.error.error)
        this.toastr.error(err.error.error, 'Ocorreu um erro');
        this.loading = false;
      }
    );
  }



  registerVeterinario() {

    console.log("Register Veterinario")
    this.loading = true;
    this.errors = [];


    if (this.password != this.passwordConfirmation) {
      this.errors.push('As Passwords não são iguais');
      this.loading = false;
      return;
    }

    if (this.morada === ""){
      this.errors.push("A morada tem de ser preenchida");
      this.loading = false;
      return;
    }

    if(this.contacto === ""){
      this.errors.push("O contacto tem de ser preenchido");
      this.loading = false;
      return;
    }

    console.log("this.role ", this.role)

    this.http.post('auth/registerVeterinario', { 
        email: this.email, 
        name: this.name, 
        password: this.password,  
        role: 'veterinario', 
        credencial: this.base64File, 
        especialidade: this.especialidade,
        horario: this.horario,
        morada: this.morada, 
        contacto: this.contacto }).subscribe(
      (res: any) => {
        this.authService.setToken(res.token);
        this.toastr.success('Conta criada com sucesso!');
        this.authService.removeToken();
        location.reload();
        this.router.navigate(["/"]);
        this.loading = false;
      },
      err => {
        console.log(err.error.error)
        this.toastr.error(err.error.error, 'Ocorreu um erro');
        this.loading = false;
      }
    );
  }


  fileChangeEvent(fileInput: any){
    //this.fileToUpload = <File> fileInput.target.file;  
    this.fileToUpload = fileInput.target.files[0];
    let reader = new FileReader();
    let data = {};
    reader.readAsDataURL(this.fileToUpload);
      reader.onload = () => {
       this.base64File = reader.result.split(',')[1]  
      };
  }




}
