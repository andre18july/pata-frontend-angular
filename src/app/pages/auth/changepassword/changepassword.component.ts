import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';



@Component({
    templateUrl: 'changepassword.component.html',
    styleUrls: ['./changepassword.component.scss']
})
export class ChangePasswordComponent {

    password: string = '';
    passwordConfirmation: string = '';
    loading: boolean = false;
    errors: Array<string> = [];


    public user = { id: '', email: '', name: '', role: '' };


    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        public authService: AuthService,
        public router: Router
    ) {}
    
    ngOnInit() {
        this.user = this.authService.getUser();
    }


    alteraPassword() {
        console.log("Dentro de altera pasword");
        console.log("user id: ",this.user.id);
        
        this.loading = true;
        this.errors = [];
    
        if (this.password != this.passwordConfirmation) {
          this.errors.push('As Passwords não são iguais');
          this.toastr.error('Erro - Passwords diferentes');
          this.loading = false;
          return;
        }
    
        this.http.put('/utilizadores/'+this.user.id+'/password', { password: this.password }).subscribe(
          (res: any) => {
            
            this.toastr.success('Password alterada com sucesso!');
            this.router.navigate(['/']);
            this.loading = false;

            this.authService.removeToken();
            this.router.navigate(['/auth/login']);
            console.log("Inside");
          },
          err => {
            for (let error of err.error.data) {
              this.errors.push(error.message);
            }
            this.loading = false;
          }
        );
    } 
}