import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAnimaisComponent } from './show-animais.component';

describe('CreateAnimaisComponent', () => {
  let component: ShowAnimaisComponent;
  let fixture: ComponentFixture<ShowAnimaisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowAnimaisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAnimaisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
