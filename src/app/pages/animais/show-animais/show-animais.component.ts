import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-show-animais',
  templateUrl: './show-animais.component.html',
  styleUrls: ['./show-animais.component.scss']
})
export class ShowAnimaisComponent implements OnInit {

  sub : Subscription;

  animalId: String = '';

  nomeAnimal: String = '';
  tipoAnimal: String = '';
  racaAnimal: String = '';
  idadeAnimal: String = '';
  fotoAnimal: String = '';

  teste: String = '';

  animal : Animal = new Animal();
  

  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public selectOptions = [
    "M",
    "F"
  ];

  public generoAnimal = '';

  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.animalId = params['id'];
    }),

    this.populateForm()
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Animal>('/animais/'+this.animalId)
    .subscribe(
      response => {
          this.animal = response;
          this.nomeAnimal = this.animal.nome;
          this.tipoAnimal = this.animal.tipo;
          this.racaAnimal = this.animal.raca;
          this.idadeAnimal = this.animal.idade;
          this.fotoAnimal = this.animal.fotoAnimal;
          this.generoAnimal = this.animal.genero.toUpperCase();
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }



  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


