import { NgModule } from '@angular/core';

import { AnimaisComponent } from './animais.component';

import { AnimaisRoutingModule } from './animais-routing.module';

import { CreateAnimaisComponent } from './create-animais/create-animais.component';
import { EditAnimaisComponent } from './edit-animais/edit-animais.component';
import { ShowAnimaisComponent } from './show-animais/show-animais.component';
import { DeleteAnimalComponent } from '../../dialogs/delete-animal/delete-animal.component';

import { SharedModule } from '../../shared/shared.module';


@NgModule({
    imports: [ AnimaisRoutingModule, SharedModule ],
    declarations: [
        AnimaisComponent,
        CreateAnimaisComponent,
        EditAnimaisComponent,
        ShowAnimaisComponent,
        DeleteAnimalComponent
    ]
})
export class AnimaisModule { }
