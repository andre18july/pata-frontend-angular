import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-create-animais',
  templateUrl: './create-animais.component.html',
  styleUrls: ['./create-animais.component.scss']
})
export class CreateAnimaisComponent implements OnInit {

  nomeAnimal: String = '';
  tipoAnimal: String = '';
  tipoAnimal2: String = '';
  racaAnimal: String = '';
  generoAnimal: String = '';
  idadeAnimal: String = '';
  fotoAnimal: string = '';
  idResponsavelAnimais: String = "";
  checkAnimal: boolean;

  //file upload
  base64File: SafeUrl = '';
  fileToUpload: File = null;

  loading       : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  constructor(public authService: AuthService, private domSanitizer: DomSanitizer, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.base64File = this.domSanitizer.bypassSecurityTrustUrl(this.fotoAnimal);

  }

  fileChangeEvent(fileInput: any){
    //this.fileToUpload = <File> fileInput.target.file;  
    this.fileToUpload = fileInput.target.files[0];
    let reader = new FileReader();
    let data = {};
    reader.readAsDataURL(this.fileToUpload);
      reader.onload = () => {
       this.base64File = reader.result.split(',')[1]  
      };
  }


  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }

  newAnimal(){
    if (this.user.role === "admin"){
      this.newAnimalAdmin();
      
    }else if(this.user.role === "responsavelanimais"){
      this.newAnimalResponsavelAnimais();
    }
  }


  newAnimalResponsavelAnimais() {

    
    console.log("Dentro de newAnimalResponsavelAnimais");
    this.loading = true;
    let userId = this.user.id;

    if(!this.generoAnimal){
      this.toastr.error('Erro: Genero vazio');
    }else{

    if(this.tipoAnimal === 'Outro'){
      this.tipoAnimal = this.tipoAnimal2;
    }

    this.http.post('/responsaveisanimais/'+userId+'/animais', { 
      nome: this.nomeAnimal, 
      tipo: this.tipoAnimal, 
      raca: this.racaAnimal, 
      genero: this.generoAnimal,
      fotoAnimal: this.base64File, 
      idade: this.idadeAnimal }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Animal registado com sucesso!');
            this.router.navigate(['/animais']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }
}


newAnimalAdmin() {

    
  console.log("Dentro de newAnimalAdmin");
  this.loading = true;


  if(!this.generoAnimal){
    this.toastr.error('Erro: Genero vazio');
  }else{

  if(this.tipoAnimal === 'Outro'){
    this.tipoAnimal = this.tipoAnimal2;
  }

  this.http.post('/animais', { 
    nome: this.nomeAnimal, 
    tipo: this.tipoAnimal, 
    raca: this.racaAnimal, 
    genero: this.generoAnimal,
    fotoAnimal: this.base64File,
    responsavel: this.idResponsavelAnimais, 
    idade: this.idadeAnimal }).subscribe(
      (res: any) => {
                    
          this.toastr.success('Animal registado com sucesso!');
          this.router.navigate(['/animais']);
          this.loading = false;
      },
      err => {
          this.toastr.error(err.error.message, 'Ocorreu um erro');
          this.loading = false;
      }
  );
}
  

}



}


