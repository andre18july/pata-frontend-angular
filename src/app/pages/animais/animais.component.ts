import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Animal } from '../../models/Animal';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DeleteAnimalComponent } from '../../dialogs/delete-animal/delete-animal.component'




@Component({
    selector: 'app-animais',
    templateUrl: 'animais.component.html'
})
export class AnimaisComponent implements OnInit { 

    modalRef: BsModalRef;
    animais : Array<Animal> = [];
    search        : string = "";
    loading       : boolean = false;
    public user = { id: '', email: '', name: '', role: '' };

    animalId: any;

    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        public authService: AuthService,
        public router: Router
    ) {}
    
    ngOnInit() {
        this.user = this.authService.getUser();
        console.log("role: ",this.user.role);
        if(this.user.role==="responsavelanimais"){
            console.log("responsavelanimais")
            this.getAnimaisDoResponsavel();
        }else if(this.user.role==="admin"){
            console.log("todos os animais")
            this.getAllAnimais();
        }else if(this.user.role==="treinador"){
            console.log("listar todos os animais ativos que tem treinos neste centro de treino sem repetidos")
            this.getAnimaisDoTreinador();
        }else if(this.user.role==="veterinario"){
            console.log("listar todos os animais ativos que tem eventos neste veterinario sem repetidos")
            this.getAnimaisDoVeterinario();
        }else{
            console.log("BINGOOOOO");
        }
    }
    
    getAnimaisDoResponsavel() {
        console.log("animais do responsavel")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Animal[]>('/responsaveisanimais/'+userId+'/animais')
        .subscribe(
            response => {
                this.loading = false;
                this.animais = response;
            },
            err => this.handleError(err)
        );
    }

    getAnimaisDoTreinador(){
        console.log("animais no centro de treino")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Animal[]>('/treinadores/'+userId+'/animais')
        .subscribe(
            response => {
                this.loading = false;
                this.animais = response;
                
            },
            err => this.handleError(err)
        );
        
    }

    
    getAnimaisDoVeterinario(){
        console.log("animais na clinica vet")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Animal[]>('/veterinarios/'+userId+'/animais')
        .subscribe(
            response => {
                this.loading = false;
                this.animais = response;
            },
            err => this.handleError(err)
        );
    }

    getAllAnimais(){
        console.log("todos os animais")
        this.loading = true;
        this.http.get<Animal[]>('/animais')
        .subscribe(
            response => {
                this.loading = false;
                this.animais = response;
            },
            err => this.handleError(err)
        );
    }

    public deleteAnimal(idAnimal: String) {
        console.log("deleteAnimal");
        console.log("idAnimal: ", idAnimal);

        //se for o responsavel animais pede permissao para eliminar com o modal
        if(this.user.role === "responsavelanimais"){
            this.modalRef = this.modalService.show(DeleteAnimalComponent, {class: 'modal-lg'});
            this.modalRef.content.animalId = idAnimal; 
            //se for o admin elimina direto
        }else if(this.user.role === "admin"){
            this.http.delete('/animais/'+idAnimal)
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Animal anonimizado com sucesso!');
                location.reload();
                this.router.navigate(["/animais"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }

    }

     
    public deleteAnimalFromDb(idAnimal: String){
        console.log("idAnimal: ", idAnimal);

        //valida que é mesmo o admin
        if(this.user.role === "admin"){
            this.http.delete('/animais/'+idAnimal+'/fromdb')
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Animal eliminado com sucesso!');
                location.reload();
                this.router.navigate(["/animais"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }

    }
     
    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
