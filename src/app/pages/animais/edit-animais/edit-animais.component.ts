import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';


@Component({
  selector: 'app-edit-animais',
  templateUrl: './edit-animais.component.html',
  styleUrls: ['./edit-animais.component.scss']
})
export class EditAnimaisComponent implements OnInit {

  sub : Subscription;

  animalId: String = '';

  nomeAnimal: String = '';
  tipoAnimal: String = '';
  tipoAnimal2: String = '';
  racaAnimal: String = '';
  idadeAnimal: String = '';
  fotoAnimal: string = '';
  responsavelAnimal: String = '';

  teste: String = '';

  animal : Animal = new Animal();
  changeFoto : boolean = false;
  

  loading : boolean = false;


  //file upload
  base64File: SafeUrl = '';
  fileToUpload: File = null;

  public user = { id: '', email: '', name: '', role: '' };
  

  public selectOptions = [
    "M",
    "F"
  ];

  public generoAnimal = '';

  constructor(public authService: AuthService, private domSanitizer: DomSanitizer, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.animalId = params['id'];
    }),

    this.base64File = this.domSanitizer.bypassSecurityTrustUrl(this.fotoAnimal);

    this.populateForm()
   
  }

  fileChangeEvent(fileInput: any){
    //this.fileToUpload = <File> fileInput.target.file;  
    this.fileToUpload = fileInput.target.files[0];
    let reader = new FileReader();
    let data = {};
    reader.readAsDataURL(this.fileToUpload);
      reader.onload = () => {
       this.base64File = reader.result.split(',')[1]  
      };
      
      this.changeFoto = true;
  }


  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Animal>('/animais/'+this.animalId)
    .subscribe(
      response => {
          this.animal = response;
          console.log("response:   ", response)
          this.nomeAnimal = this.animal.nome;
          this.tipoAnimal = this.animal.tipo;
          this.tipoAnimal2 = this.animal.tipo;
          this.racaAnimal = this.animal.raca;
          this.idadeAnimal = this.animal.idade;
          this.fotoAnimal =  this.animal.fotoAnimal,
          this.responsavelAnimal = this.animal.responsavel;
          this.generoAnimal = this.animal.genero.toUpperCase();
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }

/*
  <div class="input-group mb-4">
  <select class="form-control" name="genero" [(ngModel)]="generoAnimal">
      <option value="F">Femea</option>
      <option value="M">Macho</option>
  </select>
</div>
*/



  updateAnimal() {

    if(this.tipoAnimal === 'Outro'){
      this.tipoAnimal = this.tipoAnimal2;
    }
    
    console.log("Dentro de editAnimal");
    this.loading = true;
    let userId = this.user.id;

    if(this.changeFoto){


      this.http.put('/animais/'+this.animalId, { 
        nome: this.nomeAnimal, 
        tipo: this.tipoAnimal, 
        raca: this.racaAnimal, 
        genero: this.generoAnimal,
        fotoAnimal: this.base64File,
        responsavel: this.responsavelAnimal, 
        idade: this.idadeAnimal }).subscribe(
          (res: any) => {
                        
              this.toastr.success('Animal registado com sucesso!');
              this.router.navigate(['/animais']);
              this.loading = false;
          },
          err => {
              this.toastr.error(err.error.message, 'Ocorreu um erro');
              this.loading = false;
          }
      );
    }else{
      this.http.put('/animais/'+this.animalId, { 
        nome: this.nomeAnimal, 
        tipo: this.tipoAnimal, 
        raca: this.racaAnimal, 
        genero: this.generoAnimal,
        fotoAnimal: this.fotoAnimal,
        responsavel: this.responsavelAnimal, 
        idade: this.idadeAnimal }).subscribe(
          (res: any) => {
                        
              this.toastr.success('Animal registado com sucesso!');
              this.router.navigate(['/animais']);
              this.loading = false;
          },
          err => {
              this.toastr.error(err.error.message, 'Ocorreu um erro');
              this.loading = false;
          }
      );
    }
  }

  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }


}


