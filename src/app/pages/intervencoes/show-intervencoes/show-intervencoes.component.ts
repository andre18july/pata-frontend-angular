import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Evento } from '../../../models/Evento';
import { Intervencao } from '../../../models/Intervencao';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-show-intervencoes',
  templateUrl: './show-intervencoes.component.html',
  styleUrls: ['./show-intervencoes.component.scss']
})
export class ShowIntervencoesComponent implements OnInit {

  sub : Subscription;


  eventoId: String = '';

  intervencaoId: String = '';
  dataIntervencao: Date = null;
  descricaoIntervencao: String = '';
  eventoIntervencao: String = '';
  tipoIntervencao: String = '';
  obsIntervencao: String = '';
  ativo: String = '';

  role: String = "";
  teste: String = '';
  intervencao : Intervencao = new Intervencao();
  
  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public generoAnimal = '';

  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.intervencaoId = params['id'];
      console.log("IDDDDDDD: ", this.intervencaoId);
    }),

    this.populateForm()
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Intervencao>('intervencoes/'+this.intervencaoId+'/intervencao/detalhe')
    .subscribe(
      response => {
          this.intervencao = response;
          this.dataIntervencao = this.intervencao.data;
          this.descricaoIntervencao = this.intervencao.descricao;
          this.eventoIntervencao = this.intervencao.evento;
          this.tipoIntervencao = this.intervencao.tipo;
          this.obsIntervencao = this.intervencao.observacoes;
          this.eventoId = this.intervencao.evento;
          this.loading = false;
          console.log(response);
      },
      err => this.handleError(err)
    );
  }



  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


