import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIntervencoesComponent } from './show-intervencoes.component';

describe('ShowIntervencoesComponent', () => {
  let component: ShowIntervencoesComponent;
  let fixture: ComponentFixture<ShowIntervencoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowIntervencoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIntervencoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
