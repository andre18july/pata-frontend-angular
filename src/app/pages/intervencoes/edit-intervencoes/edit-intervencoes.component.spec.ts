import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIntervencoesComponent } from './edit-intervencoes.component';

describe('EditEventosComponent', () => {
  let component: EditIntervencoesComponent;
  let fixture: ComponentFixture<EditIntervencoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditIntervencoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIntervencoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
