import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Evento } from '../../../models/Evento';
import { Intervencao } from '../../../models/Intervencao';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-edit-intervencoes',
  templateUrl: './edit-intervencoes.component.html',
  styleUrls: ['./edit-intervencoes.component.scss']
})
export class EditIntervencoesComponent implements OnInit {

  sub : Subscription;

  eventoId: String = '';

  intervencaoId: String = '';
  eventoIntervencao: String = '';
  descricaoIntervencao: String = '';
  tipoIntervencao: String = '';
  observacoesIntervencao: String = '';


	dataIntervencao: Date = null;
	settings = {
		bigBanner: true,
		timePicker: true,
    defaultOpen: false,
    closeOnSelect: true
	}


  teste: String = '';

  intervencao : Intervencao = new Intervencao();
  evento : Evento = new Evento();

  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  


  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.intervencaoId = params['id'];
    }),

    this.populateForm()
    
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Intervencao>('/intervencoes/'+this.intervencaoId+'/intervencao/detalhe')
    .subscribe(
      response => {
          this.intervencao = response;
          this.dataIntervencao = this.intervencao.data;
          this.eventoIntervencao = this.intervencao.evento;
          this.descricaoIntervencao = this.intervencao.descricao;
          this.tipoIntervencao = this.intervencao.tipo;
          this.observacoesIntervencao = this.intervencao.observacoes;
          this.eventoId = this.intervencao.evento;
          this.loading = false;
          console.log("this.intervencao.data: ", this.intervencao.data)
      },
      err => this.handleError(err)
    );
  }




  updateIntervencao() {
    
    console.log("Dentro de editIntervencao");

    this.loading = true;
    let userId = this.user.id;
    this.http.put('/intervencoes/'+this.intervencaoId, { 
      data: this.dataIntervencao, 
      descricao: this.descricaoIntervencao,
      tipo: this.tipoIntervencao,
      evento: this.eventoIntervencao, 
      observacoes: this.observacoesIntervencao }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Intervenção registada com sucesso!');
            this.router.navigate(['/eventos', this.eventoId, 'intervencoes']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Ocorreu um erro');
            this.loading = false;
        }
    );
      

  }

  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


