import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IntervencoesComponent } from './intervencoes.component';
import { CreateIntervencoesComponent } from './create-intervencoes/create-intervencoes.component';
import { ShowIntervencoesComponent } from './show-intervencoes/show-intervencoes.component'
import { EditIntervencoesComponent } from './edit-intervencoes/edit-intervencoes.component'
import { DeleteIntervencaoComponent } from '../../dialogs/delete-intervencao/delete-intervencao.component'


import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Intervencoes'
        },
        children: [{
            path: '',
            component: IntervencoesComponent,
            pathMatch: 'full',
            data: {
                title: 'Lista Intervencoes',
                requiredRole: ['admin', 'responsavelanimais', 'veterinario']
            },
            canActivate: [AuthGuard]
        },{
            path: ':id/editar',
            data: {
                title: 'Editar Intervenção',
                requiredRole: ['admin', 'veterinario']
            },
            component: EditIntervencoesComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/eliminar',
            data: {
                title: 'Eliminar Intervenção',
                requiredRole: ['admin', 'veterinario']
            },
            component: DeleteIntervencaoComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/mostrar',
            data: {
                title: 'Mostrar Intervenção',
                requiredRole: ['admin', 'responsavelanimais','veterinario']
            },
            component: ShowIntervencoesComponent,
            canActivate: [AuthGuard]
        }]
    }
];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IntervencoesRoutingModule {}
