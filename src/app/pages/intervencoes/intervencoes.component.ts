import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Animal } from '../../models/Animal';
import { Evento } from '../../models/Evento';
import { Intervencao } from '../../models/Intervencao';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DeleteIntervencaoComponent } from '../../dialogs/delete-intervencao/delete-intervencao.component'
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-intervencoes',
    templateUrl: 'intervencoes.component.html',
    styleUrls: ['./intervencoes.component.scss']
})
export class IntervencoesComponent implements OnInit { 

    sub : Subscription;
    modalRef: BsModalRef;
    intervencoes : Array<Intervencao> = [];
    search        : string = "";
    loading       : boolean = false;
    public user = { id: '', email: '', name: '', role: '' };

    eventoId: String = "";

    dataEvento: Date = null;

    intervencaoId: String = "";
    intervencao : Intervencao = new Intervencao();

    evento : Evento = new Evento();

    constructor(
        private activeRoute: ActivatedRoute,
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        public authService: AuthService,
        public router: Router
    ) {}
    
    ngOnInit() {
        
        this.user = this.authService.getUser();

        this.sub = this.activeRoute.params.subscribe(params => {
            this.eventoId = params['id'];
        }),

        this.getAllIntervencoes();
        this.getEvento()
           
    }

    getEvento() {

        console.log("get evento")
        this.loading = true;
        this.http.get<Evento>('/eventos/'+this.eventoId)
        .subscribe(
          response => {
              this.evento = response;
              this.dataEvento = this.evento.data;
              this.loading = false;
              console.log("this.evento.data: ", this.evento.data)
          },
          err => this.handleError(err)
        );
      }


    getAllIntervencoes() {
        console.log("todas as intervencoes")
        this.loading = true;
        this.http.get<Intervencao[]>('/intervencoes/'+this.eventoId+'/'+this.user.role)
        .subscribe(
            response => {
                this.loading = false;
                this.intervencoes = response;
            },
            err => this.handleError(err)
        );
    }




    public deleteIntervencao(idIntervencao: String) {
        console.log("deleteIntervencao");
        console.log("idIntervencao: ", idIntervencao);
        if(this.user.role === "veterinario"){
            this.modalRef = this.modalService.show(DeleteIntervencaoComponent, {class: 'modal-lg'});
            this.modalRef.content.intervencaoId = idIntervencao; 
        //se for o admin elimina direto
        }else if(this.user.role === "admin"){
            this.http.delete('/intervencoes/'+idIntervencao)
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Intervencao anonimizada com sucesso!');
                location.reload();
                this.router.navigate(['/eventos', this.eventoId, 'intervencoes']);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }
    }


    
    public deleteIntervencaoFromDb(idIntervencao: String){
        console.log("idIntervencao: ", idIntervencao);

        if(this.user.role === "admin"){
            this.http.delete('/intervencoes/'+idIntervencao+'/fromdb')
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Intervenção eliminada com sucesso!');
                location.reload();
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }

    }


     
    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
