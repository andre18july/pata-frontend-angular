import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateIntervencoesComponent } from './create-intervencoes.component';

describe('CreateEventosComponent', () => {
  let component: CreateIntervencoesComponent;
  let fixture: ComponentFixture<CreateIntervencoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateIntervencoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateIntervencoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
