import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';


@Component({
  selector: 'app-create-intervencoes',
  templateUrl: './create-intervencoes.component.html',
  styleUrls: ['./create-intervencoes.component.scss']
})
export class CreateIntervencoesComponent implements OnInit {

  sub : Subscription;
  idEvento: String = '';
  descricao: String = '';
  tipo: String = '';
  observacoes: String = '';
  ativo: string = '';

	data: Date = new Date();
	settings = {
		bigBanner: true,
		timePicker: true,
    defaultOpen: false,
    closeOnSelect: true
	}


  loading       : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    console.log("dentro de newintervencao")

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.idEvento = params['id'];
    });

    console.log("id evento: ", this.idEvento);


  }


  newIntervencao() {

    
    console.log("Dentro de newIntervencao");
    this.loading = true;
    let userId = this.user.id;

   
    this.http.post('/intervencoes', { 
      data: this.data, 
      evento: this.idEvento,
      descricao: this.descricao, 
      tipo: this.tipo, 
      observacoes: this.observacoes }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Intervencao registada com sucesso!');
            this.router.navigate(['/eventos', this.idEvento, 'intervencoes']);
            this.loading = false;
        },
        err => {

            this.toastr.error(err.error.error, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }

}


