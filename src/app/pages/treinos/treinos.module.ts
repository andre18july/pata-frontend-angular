import { NgModule } from '@angular/core';
import { TreinosComponent } from './treinos.component';
import { TreinosRoutingModule } from './treinos-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { CreateTreinosComponent } from './create-treinos/create-treinos.component';
import { ShowTreinosComponent } from './show-treinos/show-treinos.component';
import { EditTreinosComponent } from './edit-treinos/edit-treinos.component';
import { DeleteTreinoComponent } from '../../dialogs/delete-treino/delete-treino.component'
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';


@NgModule({
    imports: [ TreinosRoutingModule, SharedModule, AngularDateTimePickerModule ],
    declarations: [
        TreinosComponent,
        CreateTreinosComponent,
        ShowTreinosComponent,
        EditTreinosComponent,
        DeleteTreinoComponent,
    ]
})
export class TreinosModule { }
