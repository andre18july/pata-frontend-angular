import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Animal } from '../../models/Animal';
import { Treino } from '../../models/Treino';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap';
import { DeleteTreinoComponent } from '../../dialogs/delete-treino/delete-treino.component'




@Component({
    selector: 'app-treinos',
    templateUrl: 'treinos.component.html',
    styleUrls: ['./treinos.component.scss']
})
export class TreinosComponent implements OnInit { 

    modalRef: BsModalRef;
    treinos : Array<Treino> = [];
    search        : string = "";
    loading       : boolean = false;
    public user = { id: '', email: '', name: '', role: '' };

    animalId: any;

    constructor(
        private http: HttpClient, 
        private toastr: ToastrService,
        private modalService: BsModalService,
        public authService: AuthService,
        public router: Router
    ) {}
    
    ngOnInit() {
        
        this.user = this.authService.getUser();
        
        console.log("role: ",this.user.role);
        if(this.user.role==="treinador"){
            console.log("treinador")
            this.getTreinosDoTreinador();
        }else if(this.user.role==="responsavelanimais"){
            console.log("responsavel animais");
            this.getTreinosDoRespAnimais();
        }else if(this.user.role==="admin"){
            console.log("todos os animais")
            this.getAllTreinos();
        }else{
            console.log("BINGOOOOO");
        }
        
    }
    
    getTreinosDoTreinador() {
        console.log("treinos do treinador")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Treino[]>('/treinadores/'+userId+'/treinos')
        .subscribe(
            response => {
                this.loading = false;
                this.treinos = response;
            },
            err => this.handleError(err)
        );
    }


    getTreinosDoRespAnimais() {
        console.log("treinos do animal do responsavel de animais")
        this.loading = true;
        let userId = this.user.id;
        this.http.get<Treino[]>('/responsaveisanimais/'+userId+'/treinos')
        .subscribe(
            response => {
                this.loading = false;
                this.treinos = response;
            },
            err => this.handleError(err)
        );
    }


    getAllTreinos(){
        console.log("todos os treinos")
        this.loading = true;
        this.http.get<Treino[]>('/treinos')
        .subscribe(
            response => {
                this.loading = false;
                this.treinos = response;
            },
            err => this.handleError(err)
        );
    }

    public deleteTreino(idTreino: String) {
        console.log("deleteTreino");
        console.log("idTreino: ", idTreino);

        //se for o treinador de animais pede permissao para eliminar com o modal
        if(this.user.role === "treinador"){
            this.modalRef = this.modalService.show(DeleteTreinoComponent, {class: 'modal-lg'});
            this.modalRef.content.treinoId = idTreino; 
            //se for o admin elimina direto
        }else if(this.user.role === "admin"){
            this.http.delete('/treinos/'+idTreino)
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Treino anonimizado com sucesso!');
                location.reload();
                this.router.navigate(["/treinos"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }
    }


    
    public deleteTreinoFromDb(idTreino: String){
        console.log("idTreino: ", idTreino);

        //valida que é mesmo o admin
        if(this.user.role === "admin"){
            this.http.delete('/treinos/'+idTreino+'/fromdb')
            .subscribe(
              (res: any) => {                    
                this.toastr.success('Treino eliminado com sucesso!');
                location.reload();
                this.router.navigate(["/treinos"]);
            },
            err => {
                this.toastr.error(err.error.message, 'Ocorreu um erro');
            }
            );
        }else{
            this.toastr.error('Ação negada: não tem permissões');
        }

    }


     
    private handleError(err) {
        if (this.loading) {
            this.loading = false;
        }
        this.toastr.error(err.error.message, 'Erro');
    }
}
