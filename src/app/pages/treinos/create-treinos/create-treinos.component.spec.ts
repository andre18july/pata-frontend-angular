import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTreinosComponent } from './create-treinos.component';

describe('CreateTreinosComponent', () => {
  let component: CreateTreinosComponent;
  let fixture: ComponentFixture<CreateTreinosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTreinosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTreinosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
