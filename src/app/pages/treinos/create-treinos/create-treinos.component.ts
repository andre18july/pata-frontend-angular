import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Animal } from '../../../models/Animal';

@Component({
  selector: 'app-create-treinos',
  templateUrl: './create-treinos.component.html',
  styleUrls: ['./create-treinos.component.scss']
})
export class CreateTreinosComponent implements OnInit {


  
  idtreinador: String = '';
  animal: String = '';
  tipo: String = '';
  desempenho: string = '';
  observacoes: Boolean = null;
  ativo: string = '';
  checkAnimal: boolean;

  animais : Array<Animal> = [];
  animais2 : Array<Animal> = [];

  data: Date = new Date();
	settings = {
		bigBanner: true,
		timePicker: true,
    defaultOpen: false,
    closeOnSelect: true
	}


  loading       : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  constructor(public authService: AuthService, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();

    if(this.user.role==="treinador"){
      this.getAnimaisDoTreinador();
    }
    if(this.user.role==="admin"){
      this.getAllAnimaisAtivos();
    }

  }

  newTreino(){
    if (this.user.role === "admin"){
      this.newTreinoAdmin();
      
    }else if(this.user.role === "treinador"){
      this.newTreinoTreinador();
    }
  }



  getAnimaisDoTreinador() {
    console.log("animais do treinador")
    this.loading = true;
    let userId = this.user.id;
    this.http.get<Animal[]>('/treinadores/'+userId+'/animais')
    .subscribe(
        response => {
            this.loading = false;
            this.animais = response;
        },
        err => this.handleError(err)
    );
  }

  getAllAnimaisAtivos(){
    console.log("todos os animais ativos")
    this.loading = true;
    this.http.get<Animal[]>('/animais/ativos')
    .subscribe(
        response => {
            this.loading = false;
            this.animais2 = response;
        },
        err => this.handleError(err)
    );
  }

  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



  newTreinoTreinador() {

    
    console.log("Dentro de newTreinoTreinador");
    this.loading = true;
    let userId = this.user.id;

    /*
    if(!this.generoAnimal){
      this.toastr.error('Erro: Genero vazio');
    }else{
    */

    this.http.post('/treinadores/'+userId+'/treinos', { 
      data: this.data, 
      treinador: userId,
      animal: this.animal, 
      tipo: this.tipo, 
      desempenho: this.desempenho, 
      observacoes: this.observacoes }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Treino registado com sucesso!');
            this.router.navigate(['/treinos']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.error, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }
    


  newTreinoAdmin() {

    
    console.log("Dentro de newTreinoAdmin");
    this.loading = true;
    let userId = this.user.id;

    /*
    if(!this.generoAnimal){
      this.toastr.error('Erro: Genero vazio');
    }else{
    */

    this.http.post('/treinos', { 
      data: this.data, 
      treinador: this.idtreinador,
      animal: this.animal, 
      tipo: this.tipo, 
      desempenho: this.desempenho, 
      observacoes: this.observacoes }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Treino registado com sucesso!');
            this.router.navigate(['/treinos']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.error, 'Ocorreu um erro');
            this.loading = false;
        }
    );
  }

}


