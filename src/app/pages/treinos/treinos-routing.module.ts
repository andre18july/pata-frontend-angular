import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TreinosComponent } from './treinos.component';
import { CreateTreinosComponent } from './create-treinos/create-treinos.component';
import { ShowTreinosComponent } from './show-treinos/show-treinos.component'
import { EditTreinosComponent } from './edit-treinos/edit-treinos.component'
import { DeleteTreinoComponent } from '../../dialogs/delete-treino/delete-treino.component'


import { AuthGuard } from '../../guards/auth.guard';

const routes: Routes = [
    {
        path: '',
        data: {
            title: 'Treinos'
        },
        children: [{
            path: '',
            component: TreinosComponent,
            pathMatch: 'full',
            data: {
                title: 'Lista Treinos',
                requiredRole: ['admin', 'responsavelanimais', 'treinador']
            },
            canActivate: [AuthGuard]
        },{
            path: 'criar',
            data: {
                title: 'Criar Treino',
                requiredRole: ['admin', 'treinador']
            },
            component: CreateTreinosComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/editar',
            data: {
                title: 'Editar Treino',
                requiredRole: ['admin', 'treinador']
            },
            component: EditTreinosComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/eliminar',
            data: {
                title: 'Eliminar Treino',
                requiredRole: ['admin', 'treinador']
            },
            component: DeleteTreinoComponent,
            canActivate: [AuthGuard]
        },{
            path: ':id/mostrar',
            data: {
                title: 'Mostrar Treino',
                requiredRole: ['admin', 'responsavelanimais','treinador']
            },
            component: ShowTreinosComponent,
            canActivate: [AuthGuard]
        }]
    }
];



@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TreinosRoutingModule {}
