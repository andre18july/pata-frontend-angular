import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Treino } from '../../../models/Treino'
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-edit-treinos',
  templateUrl: './edit-treinos.component.html',
  styleUrls: ['./edit-treinos.component.scss']
})
export class EditTreinosComponent implements OnInit {

  sub : Subscription;

  treinoId: String = '';

  dataTreino: Date = null;
  tipoTreino: String = '';
  observacoesTreino: String = '';

  teste: String = '';

  treino : Treino = new Treino();
  

  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public selectOptions = [
    "Fraco",
    "Médio",
    "Bom",
    "Excelente"
  ];

  public desempenhoTreino = '';

  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.treinoId = params['id'];
    }),

    this.populateForm()
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Treino>('/treinos/'+this.treinoId)
    .subscribe(
      response => {
          this.treino = response;
          
          this.dataTreino = this.treino.data;
          this.tipoTreino = this.treino.tipo;
          this.desempenhoTreino = this.treino.desempenho;
          this.observacoesTreino = this.treino.observacoes;

          this.loading = false;
      },
      err => this.handleError(err)
    );
  }


  updateTreino() {
    
    console.log("Dentro de editTreino");
    this.loading = true;
    let userId = this.user.id;
    this.http.put('/treinos/'+this.treinoId, { 
      data: this.dataTreino, 
      tipo: this.tipoTreino, 
      desempenho: this.desempenhoTreino, 
      observacoes: this.observacoesTreino }).subscribe(
        (res: any) => {
                      
            this.toastr.success('Treino registado com sucesso!');
            this.router.navigate(['/treinos']);
            this.loading = false;
        },
        err => {
            this.toastr.error(err.error.message, 'Ocorreu um erro');
            this.loading = false;
        }
    );
      

  }

  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


