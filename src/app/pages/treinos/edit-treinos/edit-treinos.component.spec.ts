import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTreinosComponent } from './edit-treinos.component';

describe('EditTreinosComponent', () => {
  let component: EditTreinosComponent;
  let fixture: ComponentFixture<EditTreinosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTreinosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTreinosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
