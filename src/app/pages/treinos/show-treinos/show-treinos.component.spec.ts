import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTreinosComponent } from './show-treinos.component';

describe('CreateAnimaisComponent', () => {
  let component: ShowTreinosComponent;
  let fixture: ComponentFixture<ShowTreinosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowTreinosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTreinosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
