import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Animal } from '../../../models/Animal';
import { Treino } from '../../../models/Treino';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-show-treinos',
  templateUrl: './show-treinos.component.html',
  styleUrls: ['./show-treinos.component.scss']
})
export class ShowTreinosComponent implements OnInit {

  sub : Subscription;

/*
    id           : string;
    data         : Date;
    treinador        : string;
    animal        : string;
    tipo      : string;
    desempenho    : string;
    observacoes:      Boolean;
    ativo   : string;

*/


  treinoId: String = '';
  dataTreino: Date = null;
  treinadorTreino: String = '';
  animalTreino: String = '';
  tipoTreino: String = '';
  desempenhoTreino: String = '';
  obsTreino: String = '';
  ativo: String = '';
  nomeAnimal: String = '';
  nomeTreinador : String = '';

  role: String = "";
  teste: String = '';
  treino : Treino = new Treino();
  
  loading : boolean = false;

  public user = { id: '', email: '', name: '', role: '' };
  

  public generoAnimal = '';

  constructor(public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {

    this.user = this.authService.getUser();

    this.sub = this.activeRoute.params.subscribe(params => {
      this.treinoId = params['id'];
      console.log("IDDDDDDD: ", this.treinoId);
    }),

    this.populateForm()
   
  }



  populateForm() {

    console.log("populate form")
    this.loading = true;
    this.http.get<Treino>('/treinos/'+this.treinoId)
    .subscribe(
      response => {
          this.treino = response;
          this.dataTreino = this.treino.data;
          this.treinadorTreino = this.treino.treinador;
          this.animalTreino = this.treino.animal;
          this.tipoTreino = this.treino.tipo;
          this.desempenhoTreino = this.treino.desempenho;
          this.obsTreino = this.treino.observacoes;
          this.nomeAnimal = this.treino.nomeAnimal;
          this.nomeTreinador = this.treino.nomeTreinador;
          this.loading = false;
      },
      err => this.handleError(err)
    );
  }



  private handleError(err) {
    if (this.loading) {
        this.loading = false;
    }
    this.toastr.error(err.error.message, 'Erro');
  }



}


