import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-delete-intervencao',
  templateUrl: './delete-intervencao.component.html',
  styleUrls: ['./delete-intervencao.component.scss']
})
export class DeleteIntervencaoComponent implements OnInit {

  sub : Subscription;
  public intervencaoId: String;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;


  public eventoId: String;

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  deleteIntervencao(){
    console.log("Intervencao IDDDD: ", this.intervencaoId);

    if(this.user.role==="veterinario"){
      this.loading = true;
      this.http.delete('/intervencoes/'+this.intervencaoId)
      .subscribe(
        (res: any) => {
                        
          this.toastr.success('Intervenção eliminada com sucesso!');
          this.bsModalRef.hide();
          document.body.classList.remove('modal-open');
          location.reload();
          this.loading = false;
      },
      err => {
          this.toastr.error(err.error.message, 'Ocorreu um erro');
          this.loading = false;
      }
      );
    }else{
      this.toastr.error('Não tem permissoes');
    }
  }

}
