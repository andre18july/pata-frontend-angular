import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteIntervencaoComponent } from './delete-intervencao.component';

describe('DeleteIntervencaoComponent', () => {
  let component: DeleteIntervencaoComponent;
  let fixture: ComponentFixture<DeleteIntervencaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteIntervencaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteIntervencaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
