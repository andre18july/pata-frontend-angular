import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteUtilizadorComponent } from './delete-utilizador.component';

describe('DeleteUtilizadorComponent', () => {
  let component: DeleteUtilizadorComponent;
  let fixture: ComponentFixture<DeleteUtilizadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteUtilizadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteUtilizadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
