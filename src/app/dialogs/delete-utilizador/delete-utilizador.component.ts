import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-delete-utilizador',
  templateUrl: './delete-utilizador.component.html',
  styleUrls: ['./delete-utilizador.component.scss']
})
export class DeleteUtilizadorComponent implements OnInit {

  sub : Subscription;
  public utilizadorId: String;
  public utilizadorRole: String;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  deleteUtilizador(){
    console.log("Utilizador IDDDD: ", this.utilizadorId);
    console.log("Utilizador role: ", this.utilizadorRole);

    /*

    if(this.user.role==="admin"){
      this.loading = true;
      this.http.delete('/utilizadores/'+this.utilizadorId)
      .subscribe(
        (res: any) => {
                        
          this.toastr.success('Utilizador eliminado com sucesso!');
          this.bsModalRef.hide();
          document.body.classList.remove('modal-open');
          location.reload();
          this.router.navigate(["/utilizadores"]);
          this.loading = false;
      },
      err => {
          this.toastr.error(err.error.message, 'Ocorreu um erro');
          this.loading = false;
      }
      );
    }else{
      this.toastr.error('Não tem permissoes');
    }
    */
  }

}
