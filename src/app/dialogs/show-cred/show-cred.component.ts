import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';
import { Utilizador } from '../../models/Utilizador';

@Component({
  selector: 'app-show-cred',
  templateUrl: './show-cred.component.html',
  styleUrls: ['./show-cred.component.scss']
})
export class ShowCredComponent implements OnInit {

  sub : Subscription;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;
  utilizador : Utilizador = new Utilizador();
  ocorrenciaId: String = '';
  credencial: String = '';

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.credencial = this.utilizador.credencial;

  }



}
