import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowCredComponent } from './show-cred.component';

describe('ShowCredComponent', () => {
  let component: ShowCredComponent;
  let fixture: ComponentFixture<ShowCredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowCredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowCredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
