import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTreinoComponent } from './delete-treino.component';

describe('DeleteTreinoComponent', () => {
  let component: DeleteTreinoComponent;
  let fixture: ComponentFixture<DeleteTreinoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTreinoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTreinoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
