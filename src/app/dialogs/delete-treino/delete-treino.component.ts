import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-delete-treino',
  templateUrl: './delete-treino.component.html',
  styleUrls: ['./delete-treino.component.scss']
})
export class DeleteTreinoComponent implements OnInit {

  sub : Subscription;
  public treinoId: String;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  deleteTreino(){
    console.log("Treino IDDDD: ", this.treinoId);

    if(this.user.role==="treinador"){
      this.loading = true;
      this.http.delete('/treinos/'+this.treinoId)
      .subscribe(
        (res: any) => {
                        
          this.toastr.success('Treino eliminado com sucesso!');
          this.bsModalRef.hide();
          document.body.classList.remove('modal-open');
          location.reload();
          this.router.navigate(["/treinos"]);
          this.loading = false;
      },
      err => {
          this.toastr.error(err.error.message, 'Ocorreu um erro');
          this.loading = false;
      }
      );
    }else{
      this.toastr.error('Não tem permissoes');
    }
  }

}
