import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-delete-evento',
  templateUrl: './delete-evento.component.html',
  styleUrls: ['./delete-evento.component.scss']
})
export class DeleteEventoComponent implements OnInit {

  sub : Subscription;
  public eventoId: String;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  deleteEvento(){
    console.log("Evento IDDDD: ", this.eventoId);

    if(this.user.role==="veterinario"){
      this.loading = true;
      this.http.delete('/eventos/'+this.eventoId)
      .subscribe(
        (res: any) => {
                        
          this.toastr.success('Evento eliminado com sucesso!');
          this.bsModalRef.hide();
          document.body.classList.remove('modal-open');
          location.reload();
          this.router.navigate(["/eventos"]);
          this.loading = false;
      },
      err => {
          this.toastr.error(err.error.message, 'Ocorreu um erro');
          this.loading = false;
      }
      );
    }else{
      this.toastr.error('Não tem permissoes');
    }
  }

}
