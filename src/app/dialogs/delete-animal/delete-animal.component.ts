import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-delete-animal',
  templateUrl: './delete-animal.component.html',
  styleUrls: ['./delete-animal.component.scss']
})
export class DeleteAnimalComponent implements OnInit {

  sub : Subscription;
  public animalId: String;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
  }

  deleteAnimal(){
    console.log("Animal IDDDD: ", this.animalId);

    if(this.user.role==="responsavelanimais"){
      this.loading = true;
      this.http.delete('/animais/'+this.animalId)
      .subscribe(
        (res: any) => {
                        
          this.toastr.success('Animal eliminado com sucesso!');
          this.bsModalRef.hide();
          document.body.classList.remove('modal-open');
          location.reload();
          this.router.navigate(["/animais"]);
          this.loading = false;
      },
      err => {
          this.toastr.error(err.error.message, 'Ocorreu um erro');
          this.loading = false;
      }
      );
    }else{
      this.toastr.error('Não tem permissoes');
    }
  }

}
