import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { BsModalRef } from 'ngx-bootstrap';
import { Ocorrencia } from '../../models/Ocorrencia';

@Component({
  selector: 'app-show-foto',
  templateUrl: './show-foto.component.html',
  styleUrls: ['./show-foto.component.scss']
})
export class ShowFotoComponent implements OnInit {

  sub : Subscription;
  public user = { id: '', email: '', name: '', role: '' };
  loading : boolean = false;
  ocorrencia : Ocorrencia = new Ocorrencia();
  ocorrenciaId: String = '';
  fotoAnimal: String = '';

  constructor(public bsModalRef: BsModalRef, public authService: AuthService, private activeRoute: ActivatedRoute, private http: HttpClient, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
    this.user = this.authService.getUser();
    this.fotoAnimal = this.ocorrencia.fotoAnimal;

  }



}
