export class Ocorrencia {
    id: string;
    data: Date;
    tipo: string;
    animal: string;
    responsavelAnimais: string;
    descricao: string;
    localizacao: string;
    lat: string;
    long: string;
    estadoAnimal: string;
    tipoAnimal: string;
    racaAnimal: string;
    fotoAnimal: string;
    contacto: string;
    agressor: string;
    notificada: Boolean;
    ativo:      Boolean;
}