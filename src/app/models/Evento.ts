export class Evento {
    id           : string;
    data         : Date;
    veterinario        : string;
    animal        : string;
    tipo      : string;
    observacoes:      string;
    ativo   : string;
    nomeAnimal: string;
    nomeVeterinario: string;
}