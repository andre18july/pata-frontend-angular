export class Animal {
    id           : string;
    nome         : string;
    tipo        : string;
    raca        : string;
    genero      : string;
    idade    : string;
    fotoAnimal: string;
    responsavel: string;
    ativo:      Boolean;

}