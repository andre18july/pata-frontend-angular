export class Treino {
    id           : string;
    data         : Date;
    treinador        : string;
    animal        : string;
    tipo      : string;
    desempenho    : string;
    observacoes:      string;
    ativo   : string;
    nomeAnimal: string ;
    nomeTreinador : string ;
}