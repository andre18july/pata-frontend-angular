export * from './Animal';
export * from './Comentario';
export * from './User';
export * from './Evento';
export * from './Ocorrencia';
export * from './Treino';
export * from './Utilizador';
export * from './Intervencao';
