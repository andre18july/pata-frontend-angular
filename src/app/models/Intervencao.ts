export class Intervencao {
    id           : string;
    data         : Date;
    evento        : string;
    descricao: String;
    tipo      : string;
    observacoes:      string;
    ativo   : string;
}